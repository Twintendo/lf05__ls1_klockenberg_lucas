import java.util.Scanner; // Import der Klasse Scanner

public class nameUndAlter {

	  public static void main(String[] args) // Hier startet das Programm 
	  { 
	     
	    // Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	    
	    System.out.println("Bitte geben Sie Ihren Namen ein: ");
	    
	    String name = myScanner.next();
	    
	    System.out.println("Ihr Name ist: " + name + ".");
	    
	    System.out.println("Bitte geben Sie Ihr Alter ein: ");
	    
	    int alter = myScanner.nextInt();
	    
	    System.out.println("Sie sind: " + alter + " Jahre alt.");

	}

}
