
import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner1  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    //Addition
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    int ergebnisA = zahl1 + zahl2;  
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnisA);   
    
    
    
    
    //Subtraktion
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
    
    // Die Variable zahl3 speichert die erste Eingabe 
    int zahl3 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl4 speichert die zweite Eingabe 
    int zahl4 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    int ergebnisS = zahl3 - zahl4;  

    System.out.print("\n\n\nErgebnis der Subtraktion lautet: "); 
    System.out.println("\n" + zahl3 + " - " + zahl4 + " = " + ergebnisS);   
    
    
    
    
    //Division
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
    
    // Die Variable zahl5 speichert die erste Eingabe 
    int zahl5 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl6 speichert die zweite Eingabe 
    int zahl6 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    double ergebnisD = zahl5/zahl6;  
     
    System.out.print("\n\n\nErgebnis der Division lautet: "); 
    System.out.println("\n" + zahl5 + " / " + zahl6 + " = " + ergebnisD);   
    
    
    
    
    //Mulitplikation
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
    
    // Die Variable zahl7 speichert die erste Eingabe 
    int zahl7 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl8 speichert die zweite Eingabe 
    int zahl8 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    int ergebnisM = zahl7 * zahl8;  
     
    System.out.print("\n\n\nErgebnis der Multiplikation lautet: "); 
    System.out.println("\n" + zahl7 + " * " + zahl8 + " = " + ergebnisM);   
    
    myScanner.close(); 
     
  }    
}