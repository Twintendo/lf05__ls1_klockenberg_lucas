
public class Aufgabe_2 {

	static void berechneFakultaet(int zahl) {
		int fakultaet = 1;
		for (int i = 1; i <= zahl; ++i) {
			fakultaet = fakultaet * i;
			}
		System.out.printf("%4s \n", fakultaet);
		
	}
	public static void main(String[] args) {
		//Erste Zeile
		System.out.printf("%-5s", "0!");
		System.out.printf("%s", " = ");
		System.out.printf("%-19s", " ");
		System.out.printf("%s", " = ");
		berechneFakultaet(0);
		
		//Zweite Zeile
		System.out.printf("%-5s", "1!");
		System.out.printf("%s", " = ");
		System.out.printf("%-19s", "1");
		System.out.printf("%s", " = ");
		berechneFakultaet(1);
		
		//Dritte Zeile
		System.out.printf("%-5s", "2!");
		System.out.printf("%s", " = ");
		System.out.printf("%-19s", "1 * 2");
		System.out.printf("%s", " = ");
		berechneFakultaet(2);
		
		//Vierte Zeile
		System.out.printf("%-5s", "3!");
		System.out.printf("%s", " = ");
		System.out.printf("%-19s", "1 * 2 *3");
		System.out.printf("%s", " = ");
		berechneFakultaet(3);
		
		//F�nfte Zeile
		System.out.printf("%-5s", "4!");
		System.out.printf("%s", " = ");
		System.out.printf("%-19s", "1 * 2 * 3 * 4");
		System.out.printf("%s", " = ");
		berechneFakultaet(4);
		
		//Sechste Zeile
		System.out.printf("%-5s", "5!");
		System.out.printf("%s", " = ");
		System.out.printf("%-19s", "1 * 2 * 3 * 4 * 5");
		System.out.printf("%s", " = ");
		berechneFakultaet(5);
		

	}

}
