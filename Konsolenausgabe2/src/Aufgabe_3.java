public class Aufgabe_3 {

	public static void main(String[] args) {
		//Überschrift
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s \n", "Celsius");
		
		//Zeile 1
		System.out.println("-----------------------");
		
		//Zeile 2
		System.out.printf("%-12.6s", "-20");
		System.out.print("|");
		System.out.printf("%10.6s \n", "-28.89");
		
		//Zeile 3
		System.out.printf("%-12.6s", "-10");
		System.out.print("|");
		System.out.printf("%10.6s \n", "-23.33");
		
		//Zeile 4
		System.out.printf("%-12.6s", "0");
		System.out.print("|");
		System.out.printf("%10.6s \n", "-17.78");
		
		//Zeile 5
		System.out.printf("%-12.6s", "20");
		System.out.print("|");
		System.out.printf("%10.5s \n", "-6.67");
		
		//Zeile 6
		System.out.printf("%-12.6s", "30");
		System.out.print("|");
		System.out.printf("%10.5s \n", "-1.11");
		

	}

}
