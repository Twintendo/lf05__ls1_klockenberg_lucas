import java.util.ArrayList;
import java.util.Random;

/**
 * @author lucas.klockenberg
 * @since 18.05.2022
 */
public class Raumschiff 
{
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	
	/**
	 * Erstellt ein Objekt der Klasse Raumschiff.
	 * Daf�r werden die einzelnen Parameter in dem Konstruktor erw�hnt.
	 * @param schiffsname
	 * @param androidenAnzahl
	 * @param lebenserhaltungssystemeInProzent
	 * @param huelleInProzent
	 * @param schildeInProzent
	 * @param energieversorgungInProzent
	 * @param photonentorpedoAnzahl
	 */
	 
	public Raumschiff(String schiffsname, int androidenAnzahl, int lebenserhaltungssystemeInProzent,
					int huelleInProzent, int schildeInProzent, int energieversorgungInProzent, 
					int photonentorpedoAnzahl)	
	{
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		
	}
	
	/**
	 * Der getter f�r das Attribut photonentorpedoAnzahl.
	 * @return int
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * Der setter f�r das Attribut photonentorpedoAnzahl
	 * @param photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	/**
	 * Der getter f�r das Attribut energieversorgungInProzent.
	 * @return int
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * Der setter f�r das Attribut energieversorgungInProzent.
	 * @param energieversorgungInProzent
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * Der getter f�r das Attribut schildeInProzent.
	 * @return int
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * Der setter f�r das Attribut schildeInProzent.
	 * @param schildeInProzent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	/**
	 * Der getter f�r das Attribut huelleInProzent.
	 * @return int
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * Der setter f�r das Attribut huelleInProzent.
	 * @param huelleInProzent
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	/**
	 * Der getter f�r das Attribut lebenserhaltungssystemeInProzent.
	 * @return int
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * Der setter f�r das Attribut lebenserhaltungssystemeInProzent.
	 * @param lebenserhaltungssystemeInProzent
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	/**
	 * Der getter f�r das Attribut androidenAnzahl.
	 * @return int
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * Der setter f�r das Attribut androidenAnzahl.
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * Der getter f�r das Attribut schiffsname.
	 * @return String
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	 * Der setter f�r das Attribut schiffsname.
	 * @param schiffsname
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	//Weitere Methoden
	
	/**
	 * �ber diese Methode werden neue Ladungen erstellt. 
	 * @param ladung
	 */
	public void addLadung(Ladung ladung)
	{
		this.ladungsverzeichnis.add(ladung);
	}
	
	/**
	 * �ber diese Methode wird ein Photonentorpedo auf ein bestimmtes Ziel geschossen.
	 * @param ziel
	 */
	public void photonentorpedoSchiessen(Raumschiff ziel)
	{
		
		if(photonentorpedoAnzahl == 0)
		{
			nachrichtAnAlle(schiffsname + ": Keine Photonentorpedos gefunden!");
			nachrichtAnAlle(schiffsname + ": -=*Click*=-");
		}
		if(photonentorpedoAnzahl >= 1)
		{
			nachrichtAnAlle(schiffsname + ": Photonentorpedo abgeschossen!");
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			treffer(ziel);
		}
		
	}
	
	/**
	 * �ber diese Methode wird die Phaserkanone auf ein bestimmtes Ziel geschossen.
	 * @param ziel
	 */
	public void phaserkanonenSchiessen(Raumschiff ziel)
	{
		if(energieversorgungInProzent < 50)
		{
			nachrichtAnAlle(schiffsname + ": -=*Click*=-");;
		}
		if(energieversorgungInProzent >= 50)
		{
			nachrichtAnAlle(schiffsname + ": Phaserkanonen abgeschossen!");
			energieversorgungInProzent = energieversorgungInProzent - 50;
			treffer(ziel);
		}
		
	}
	
	/**
	 * �ber diese Methode wird bei einem Treffer eine Nachricht ausgegeben, welche aussagt, welches Schiff getroffen wurde.
	 * <p> 
	 * Es wird ebenfalls bestimmt, wie viel Schaden jedes Raumschiff nach einem Treffer erh�lt.
	 * @param gegner
	 */
	private void treffer(Raumschiff gegner)
	{
		nachrichtAnAlle(gegner.schiffsname + " wurde getroffen!");
		if(gegner.schildeInProzent > 50)
		{
			gegner.schildeInProzent = gegner.schildeInProzent - 50;
		}
		else if(gegner.schildeInProzent <= 50)
		{
			gegner.schildeInProzent = 0;
			if(gegner.huelleInProzent > 50)
			{
				gegner.huelleInProzent = gegner.huelleInProzent - 50;
				gegner.energieversorgungInProzent = gegner.energieversorgungInProzent - 50;
			}
			else
			{
				gegner.huelleInProzent = 0;
				gegner.energieversorgungInProzent = 0;
				nachrichtAnAlle("Die Lebenserhaltungssysteme von " + gegner.schiffsname + " wurden vollst�ndig zerst�rt.");
				gegner.lebenserhaltungssystemeInProzent = 0;
			}
		}
	
	}
	
	/**
	 * Diese Methode ist die allgemeine Methode um eine Nachricht ausgeben zu lassen.
	 * @param message
	 */
	public void nachrichtAnAlle(String message)
	{
		System.out.println(message);
		addLogbuchEintrag(message);
	}
	
	 /**
	  * �ber diese Methode werden die Torpedos eines Raumschiffs nachgeladen.
	 * <p>
	 * Die Anzahl des Attributs photonentorpedoAnzahl wird erh�ht.
	  * @param raumschiff
	  * @param ladung
	  */
	public void photonentorpedosLaden(Raumschiff raumschiff, Ladung ladung)
	{
		if(ladung.getMenge() == 0)
		{
			nachrichtAnAlle(raumschiff.schiffsname + ": Keine Photonentorpedos gefunden!");
		}
		raumschiff.photonentorpedoAnzahl = raumschiff.photonentorpedoAnzahl + ladung.getMenge();
		ladung.setMenge(ladung.getMenge() - ladung.getMenge());
		System.out.println(schiffsname + ": " + ladung.getMenge() + " Photonentorpedos wurden nachgeladen.");
	}
	
	/**
	 * �ber diese Methode wird eine Reparatur an einem Raumschiff durchgef�hrt.
	 * <p>
	 * Die Zahl der Parameter der Methode wird um einen Zufallswert erh�ht.
	 * @param raumschiff
	 */
	public void reparaturDurchfuehren(Raumschiff raumschiff)
	{
		int anzahlDroiden;
		anzahlDroiden = raumschiff.androidenAnzahl;
		int counter = 0;
		int min = 1;
		int max = 100;

		Random random = new Random();

		int value = random.nextInt(max + min) + min;
		
		
		if(anzahlDroiden > raumschiff.androidenAnzahl)
		{
			anzahlDroiden = raumschiff.androidenAnzahl;
		}
		
		if(raumschiff.schildeInProzent > 0)
		{
			counter = counter + 1;
		}
		else
		{
		}
		if(raumschiff.energieversorgungInProzent > 0)
		{
			counter = counter + 1;
		}
		else
		{
		}
		if(raumschiff.huelleInProzent > 0)
		{
			counter = counter + 1;
		}
		else
		{
		}
		if(counter >= 1)
		{
			raumschiff.schildeInProzent = (value*anzahlDroiden)/counter;
			raumschiff.energieversorgungInProzent = (value*anzahlDroiden)/counter;
			raumschiff.huelleInProzent = (value*anzahlDroiden)/counter;
		}
		else if(counter == 0)
		{
			nachrichtAnAlle("Reparater bei: " + schiffsname + " nicht m�glich, da das Schiff zerst�rt wurde.");
		}
	}
	
	/**
	 * �ber diese Methode kann man sich den Zustand eines einzelnen Raumschiffs azeigen lassen.
	 */
	public void zustandRaumschiff()
	{
		System.out.println("Name: " + schiffsname);
		System.out.println("Androide-Anzahl: " + androidenAnzahl);
		System.out.println("Energieversorung in Prozent: " + energieversorgungInProzent);
		System.out.println("Schilde in Prozent: " + schildeInProzent);
		System.out.println("Huelle in Prozent: " + huelleInProzent);
		System.out.println("Lebenserhaltungssysteme in Prozent: " + lebenserhaltungssystemeInProzent);
		System.out.println("Anzahl an Photonentorpedos: " + photonentorpedoAnzahl);
	}
	/**
	 * �ber diese Methode kann man sich das Ladungsverzeichnis eines Raumschiffs anzeigen lassen.
	 */
	public void ladungsverzeichnisAusgeben()
	{
		System.out.println(schiffsname + " Ladung: " + ladungsverzeichnis);;
	}
	
	/**
	 * �ber diese Methode wird eine Ladung des Ladungsverzeichnisses entfernt.
	 * @param ladung
	 * @param raumschiff
	 */
	public void ladungsverzeichnisAufraeumen(Ladung ladung, Raumschiff raumschiff)
	{
		this.ladungsverzeichnis.remove(ladung);
	}
	
	/**
	 * �ber diese Methode werden alle Eintr�ge/Nachrichten, die mit dem gew�hlten Raumschiff zu tun hatten angezeigt.
	 */
	public void broadcastKommunikatorAnzeigen()
	{
		System.out.println(schiffsname + " Logbuch: " + broadcastKommunikator);
	}
	
	/**
	 * Diese Methode f�gt dem Logbuch Nachrichten hinzu, direkt nachdem diese gesendet wurde. 
	 * <p>
	 * Siehe Methode "nachrichtAnAlle".
	 * @param broadcastKommunikator
	 */
	public void addLogbuchEintrag(String broadcastKommunikator)
	{
		this.broadcastKommunikator.add(broadcastKommunikator);
	}
	
	
}//Class Ende
