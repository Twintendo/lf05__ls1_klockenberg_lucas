import java.util.Random;

public class Ausfuehrung {

	public static void main(String[] args) {
		Ladung ldg1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ldg2 = new Ladung("Borg-Schrott", 5);
		Ladung ldg3 = new Ladung("Rote Materie", 2);
		Ladung ldg4 = new Ladung("Forschungssonde", 35);
		Ladung ldg5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung ldg6 = new Ladung("Plasma-Waffe", 50);
		Ladung ldg7 = new Ladung("Photonentorpedo", 3);
		
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 2, 100, 100, 100, 100, 1);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Vai", 5, 80, 80, 50, 100, 0);
		
		klingonen.addLadung(ldg1);
		klingonen.addLadung(ldg5);
		
		romulaner.addLadung(ldg2);
		romulaner.addLadung(ldg3);
		romulaner.addLadung(ldg6);
		
		vulkanier.addLadung(ldg4);
		vulkanier.addLadung(ldg7);
		
		System.out.println("Eine detaillierte Beschreibung, des Klingonenschiffes:");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.printf("\n\n");
		
		System.out.println("Eine detaillierte Beschreibung, des Romulanerschiffes:");
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		System.out.printf("\n\n");
		
		System.out.println("Eine detaillierte Beschreibung, des Vulkanierschiffes:");
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.printf("\n\n");
		
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.printf("\n");
		
		romulaner.phaserkanonenSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Vulkanier: Gewalt ist nicht logisch");
		System.out.printf("\n");
		
		klingonen.zustandRaumschiff();
		System.out.printf("\n");
		klingonen.ladungsverzeichnisAusgeben();
		System.out.printf("\n");
		vulkanier.reparaturDurchfuehren(vulkanier);
		vulkanier.photonentorpedosLaden(vulkanier, ldg7);
		vulkanier.ladungsverzeichnisAufraeumen(ldg7, vulkanier);
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.printf("\n");
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.printf("\n");
		klingonen.zustandRaumschiff();
		System.out.printf("\n");
		klingonen.ladungsverzeichnisAusgeben();
		System.out.printf("\n");
		romulaner.zustandRaumschiff();
		System.out.printf("\n");
		romulaner.ladungsverzeichnisAusgeben();
		System.out.printf("\n");
		vulkanier.zustandRaumschiff();
		System.out.printf("\n");
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.printf("\n");
		System.out.printf("\n");
		
		klingonen.broadcastKommunikatorAnzeigen();
		System.out.printf("\n");
		romulaner.broadcastKommunikatorAnzeigen();
		System.out.printf("\n");
		vulkanier.broadcastKommunikatorAnzeigen();
		System.out.printf("\n");
	}

}
