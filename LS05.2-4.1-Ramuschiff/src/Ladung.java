
public class Ladung 
{
	private String bezeichnung;
	private int menge;
	
	/**
	 * Erstellt ein Objekt der Klasse Ladung.
	 * @param bezeichnung
	 * @param menge
	 */
	public Ladung(String bezeichnung, int menge)
	{
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	/**
	 * Der getter f�r das Attribut bezeichnung
	 * @return String
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}
	
	/**
	 * Der setter f�r das Attribut bezeichnung.
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	/**
	 * Der getter f�r das Attribut menge.
	 * @return int
	 */
	public int getMenge() {
		return menge;
	}
	
	/**
	 * Der setter f�r das Attribut menge.
	 * @param menge
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}

	
	@Override
	public String toString() 
	{
		return this.bezeichnung + "("+this.menge+")";
	}
	
	
} //Class Ende
