import java.util.Scanner;


class Fahrkartenautomat
{
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       double ticketAnzahl;

       System.out.println("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = fahrkartenBestellungErfassen();
       
       System.out.print("Geben sie die gew�nschte Anzahl an Tickets an: ");
       ticketAnzahl = fahrkartenBestellungErfassen();

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0; 
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag * ticketAnzahl)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro", + (zuZahlenderBetrag * ticketAnzahl - eingezahlterGesamtbetrag));
    	   System.out.printf("\nEingabe (mind. 5Ct, h�chstens 5 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }
       
       System.out.printf("Ihr Wechselgeld betr�gt: %.2f", fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag, ticketAnzahl));
      

       // Fahrscheinausgabe
       // -----------------
       
       fahrkartenAusgeben();

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag * ticketAnzahl;
       
       System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro wird in folgenden M�nzen ausgezahlt: \n", rueckgeldAusgeben(r�ckgabebetrag));
       if(r�ckgabebetrag > 0.0)
       {
    	   r�ckgabebetrag = Math.round(r�ckgabebetrag * 100)/100.0;
    	   /*System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro ", + r�ckgabebetrag);
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");*/

           while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.00;
	          r�ckgabebetrag = Math.round(r�ckgabebetrag * 100)/100.0;
           }
           while(r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.00;
	          r�ckgabebetrag = Math.round(r�ckgabebetrag * 100)/100.0;
           }
           while(r�ckgabebetrag >= 0.50) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.50;
	          r�ckgabebetrag = Math.round(r�ckgabebetrag * 100)/100.0;
	          
           }
           while(r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.20;
 	          r�ckgabebetrag = Math.round(r�ckgabebetrag * 100)/100.0;
           }
           while(r�ckgabebetrag >= 0.10) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.10;
	          r�ckgabebetrag = Math.round(r�ckgabebetrag * 100)/100.0;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	 System.out.println("5 CENT");
 	         r�ckgabebetrag -= 0.05;
 	         r�ckgabebetrag = Math.round(r�ckgabebetrag * 100)/100.0;
           }
       }
       
       

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }

    public static double fahrkartenBestellungErfassen()
    {
    	Scanner myScanner = new Scanner(System.in);
    	double fahrkarte = myScanner.nextDouble();
    	return fahrkarte;
    }

    public static void warte(int millisekunde) 
    {
    	return ;
    }


    public static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag, double ticketAnzahl)
    {
    	double wechselgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag * ticketAnzahl;
    	return wechselgeld;
    }

    public static void fahrkartenAusgeben()
    {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }

    public static double rueckgeldAusgeben(double r�ckgabebetrag)
    {
    	double wechselgeld = r�ckgabebetrag;
    	return wechselgeld;
    }
  
}