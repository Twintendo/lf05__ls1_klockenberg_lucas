
import java.util.Scanner;

public class LS05_1_A4_5Farhkartenautomat_Endlosmodus {

	public static void main(String[] args) {

				Scanner tastatur = new Scanner(System.in);

				double zuZahlenderBetrag;
				double eingezahlterGesamtbetrag;
				double eingeworfeneM�nze;
				double r�ckgabebetrag;
				double anzahlTicket;
				
				while(true) //Endlosmodus
				{
				
				System.out.println("W�hlen Sie ein Ticket: ");
				
				System.out.print("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n" +
								 "Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n" +
								 "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
				zuZahlenderBetrag = fahrkartenBestellungErfassen();

				System.out.print("Geben sie die gew�nschte Anzahl an Tickets an:");
				anzahlTicket = ticketAnzahlErfassen();

				// Geldeinwurf
				// -----------
				eingezahlterGesamtbetrag = 0.0;
				while (eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahlTicket) 
				{
					System.out.printf("Noch zu zahlen: %.2f Euro",
							(zuZahlenderBetrag * anzahlTicket - eingezahlterGesamtbetrag));
					System.out.printf("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
					eingeworfeneM�nze = tastatur.nextDouble();
					eingezahlterGesamtbetrag += eingeworfeneM�nze;
				}
				

				System.out.printf("Ihr Wechselgeld betr�gt: %.2f",
						fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag, anzahlTicket));

				// Fahrscheinausgabe
				// -----------------
				fahrkartenAusgeben();

				// R�ckgeldberechnung und -Ausgabe
				// -------------------------------
				r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag * anzahlTicket;
				warte(2500);
				System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro wird in folgenden M�nzen ausgezahlt: \n",
						rueckgeldAusgeben(r�ckgabebetrag));
				muenzeAusgeben(r�ckgabebetrag);

				warte(1500);
				System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
						+ "Wir w�nschen Ihnen eine gute Fahrt.");
				}
	}

			// Methoden Fahrkartenautomat Aufgabe 3.3
			public static double fahrkartenBestellungErfassen() {
				{
					Scanner myScanner = new Scanner(System.in);
					int ticket;
					double preis;
					boolean ticketUeberpruefung = true;
					
					//Ticketwahl
					do 
					{
						ticket = myScanner.nextInt();
						
						System.out.println("Ihre Wahl: " + ticket);
						
						if(ticket > 0 && ticket < 4)
						{
							ticketUeberpruefung = false;
						}
						else
							System.out.println("Falsche Eingabe");
					}
					while (ticketUeberpruefung);
					
					if(ticket == 1)
					{
						preis = 2.90;
						return preis;
					}
					else if (ticket == 2)
					{
						preis = 8.60;
						return preis;
					}
					else if (ticket == 3)
					{
						preis = 23.50;
						return preis;
					}
					
					
					return ticket;
					
				}
			}
			
			//Begrenzte Tickets --> Methode (Aufgabe 4.2)
			public static double ticketAnzahlErfassen ()
			{
				Scanner myScanner = new Scanner(System.in);
				double fahrkarte = myScanner.nextDouble();
				if(fahrkarte >= 1 && fahrkarte <= 10)
				{
					
				}
				else
				{
					System.out.println("Die Anzahl " + fahrkarte + " wird nicht zugelassen. Der Wert wird auf '1' Fahrkarte gesetzt!");
					fahrkarte = 1;
				}
				return fahrkarte;
			}

			// warte-Methode A3.4
			public static void warte(long millisekunde) {
				try {
					Thread.sleep(millisekunde);
				} catch (InterruptedException ignored) {

				}

			}

			public static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag,
					double ticketAnzahl) {
				double wechselgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag * ticketAnzahl;
				return wechselgeld;
			}

			public static void fahrkartenAusgeben() {
				System.out.println("\nFahrschein wird ausgegeben");
				for (int i = 0; i < 8; i++) {
					System.out.print("=");
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				System.out.println("\n\n");
			}

			public static double rueckgeldAusgeben(double r�ckgabebetrag) {
				double wechselgeld = r�ckgabebetrag;
				return wechselgeld;
			}

			//Aufgabe A3.4
			public static void muenzeAusgeben(double r�ckgabebetrag) {
				if (r�ckgabebetrag > 0.0) {
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					/*
					 * System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro ",
					 * r�ckgabebetrag, " EURO ");
					 * System.out.println("wird in folgenden M�nzen ausgezahlt:");
					 */

					while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
					{
						System.out.println("2 EURO");
						r�ckgabebetrag -= 2.0;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
					{
						System.out.println("1 EURO");
						r�ckgabebetrag -= 1.0;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
					{
						System.out.println("50 CENT");
						r�ckgabebetrag -= 0.5;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
					{
						System.out.println("20 CENT");
						r�ckgabebetrag -= 0.2;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
					{
						System.out.println("10 CENT");
						r�ckgabebetrag -= 0.1;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
					{
						System.out.println("5 CENT");
						r�ckgabebetrag -= 0.05;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
				}
			
			}
		}